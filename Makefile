# find all .md files in the directory
# mdsrc=$(shell ls *.md)
mdsrc=$(shell find . -iname "*.md")
# mdsrc=$(wildcard *.md */*.md **/*.md )
# map *.mp => *.html for mdsrc
html_from_md=$(mdsrc:%.md=%.html)
docx_from_md=$(mdsrc:%.md=%.docx)

all: $(html_from_md) $(docx_from_md)

fixnames:
	rename "s/ /_/g" *

today:
	touch `date +"%Y-%m-%d.md"`

now_folder:
	mkdir `date +"%Y-%m-%d-%H%M%S"`

# Implicit rule to know how to make .html from .md
%.docx: %.md
	pandoc --from markdown \
		--to docx \
		--standalone \
		--smart \
		--section-divs \
		--css styles.css $< -o $@

%.html: %.md
	pandoc --from markdown \
		--to html \
		--standalone \
		--smart \
		--section-divs \
		--css styles.css $< -o $@

# special rule for debugging variables
print-%:
	@echo '$*=$($*)'
